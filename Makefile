PROJECT_NAME     := app
OUTPUT_DIRECTORY := _build

# Main Variables
PROJ_DIR := .
SDK_ROOT := $(PROJ_DIR)/_sdk
SDK_CONFIG := $(PROJ_DIR)/sdk_config
DFU_DIR := $(PROJ_DIR)/dfu
OTA_DIR := $(PROJ_DIR)/_ota
MAKE_PATH := $(PROJ_DIR)/make

PROG_SER := 682978319
SOFT_DEVICE := $(SDK_ROOT)/components/softdevice/s112/hex/s112_nrf52810_5.1.0_softdevice.hex

$(shell mkdir -p "$(OTA_DIR)")

# Includ getting version from version.json:
include $(MAKE_PATH)/Makefile.version

# Bootloader related items
BL_DIR := $(PROJ_DIR)/bootloader
BL_BIN_DIR := $(BL_DIR)/_build
BL_HASH := 07f4131
BL_NAME := bl_$(VER_STRING)_$(BL_HASH)

# Variables related to the sdk
NRF_SDK_URL := https://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v14.x.x/nRF5_SDK_14.2.0_17b948a.zip
NRF_SDK_MD5 := c66abfe64a52ffcc06a55a437a67e03f

$(OUTPUT_DIRECTORY)/$(TARGET_NAME).out: \
  LINKER_SCRIPT  := ble_app_buttonless_dfu_gcc_nrf52.ld

# Source files common to all targets
SRC_FILES += \
  $(SDK_ROOT)/components/libraries/experimental_log/src/nrf_log_backend_rtt.c \
  $(SDK_ROOT)/components/libraries/experimental_log/src/nrf_log_backend_serial.c \
  $(SDK_ROOT)/components/libraries/experimental_log/src/nrf_log_backend_uart.c \
  $(SDK_ROOT)/components/libraries/experimental_log/src/nrf_log_default_backends.c \
  $(SDK_ROOT)/components/libraries/experimental_log/src/nrf_log_frontend.c \
  $(SDK_ROOT)/components/libraries/experimental_log/src/nrf_log_str_formatter.c \
  $(SDK_ROOT)/components/boards/boards.c \
  $(SDK_ROOT)/components/libraries/button/app_button.c \
  $(SDK_ROOT)/components/libraries/util/app_error.c \
  $(SDK_ROOT)/components/libraries/util/app_error_weak.c \
  $(SDK_ROOT)/components/libraries/scheduler/app_scheduler.c \
  $(SDK_ROOT)/components/libraries/timer/app_timer.c \
  $(SDK_ROOT)/components/libraries/util/app_util_platform.c \
  $(SDK_ROOT)/components/libraries/crc16/crc16.c \
  $(SDK_ROOT)/components/libraries/fds/fds.c \
  $(SDK_ROOT)/components/libraries/util/nrf_assert.c \
  $(SDK_ROOT)/components/libraries/atomic_fifo/nrf_atfifo.c \
  $(SDK_ROOT)/components/libraries/balloc/nrf_balloc.c \
  $(SDK_ROOT)/external/fprintf/nrf_fprintf.c \
  $(SDK_ROOT)/external/fprintf/nrf_fprintf_format.c \
  $(SDK_ROOT)/components/libraries/fstorage/nrf_fstorage.c \
  $(SDK_ROOT)/components/libraries/fstorage/nrf_fstorage_sd.c \
  $(SDK_ROOT)/components/libraries/experimental_memobj/nrf_memobj.c \
  $(SDK_ROOT)/components/libraries/pwr_mgmt/nrf_pwr_mgmt.c \
  $(SDK_ROOT)/components/libraries/experimental_section_vars/nrf_section_iter.c \
  $(SDK_ROOT)/components/libraries/strerror/nrf_strerror.c \
  $(SDK_ROOT)/components/libraries/util/sdk_mapped_flags.c \
  $(SDK_ROOT)/components/drivers_nrf/clock/nrf_drv_clock.c \
  $(SDK_ROOT)/components/drivers_nrf/common/nrf_drv_common.c \
  $(SDK_ROOT)/components/drivers_nrf/gpiote/nrf_drv_gpiote.c \
  $(SDK_ROOT)/components/drivers_nrf/uart/nrf_drv_uart.c \
  $(SDK_ROOT)/components/libraries/bsp/bsp.c \
  $(SDK_ROOT)/components/libraries/bsp/bsp_btn_ble.c \
  $(SDK_ROOT)/components/libraries/bsp/bsp_nfc.c \
  $(PROJ_DIR)/main.c \
  $(SDK_ROOT)/components/libraries/bootloader/dfu/nrf_dfu_svci.c \
  $(SDK_ROOT)/external/segger_rtt/SEGGER_RTT.c \
  $(SDK_ROOT)/external/segger_rtt/SEGGER_RTT_Syscalls_GCC.c \
  $(SDK_ROOT)/external/segger_rtt/SEGGER_RTT_printf.c \
  $(SDK_ROOT)/components/ble/common/ble_advdata.c \
  $(SDK_ROOT)/components/ble/ble_advertising/ble_advertising.c \
  $(SDK_ROOT)/components/ble/common/ble_conn_params.c \
  $(SDK_ROOT)/components/ble/common/ble_conn_state.c \
  $(SDK_ROOT)/components/ble/common/ble_srv_common.c \
  $(SDK_ROOT)/components/ble/peer_manager/gatt_cache_manager.c \
  $(SDK_ROOT)/components/ble/peer_manager/gatts_cache_manager.c \
  $(SDK_ROOT)/components/ble/peer_manager/id_manager.c \
  $(SDK_ROOT)/components/ble/nrf_ble_gatt/nrf_ble_gatt.c \
  $(SDK_ROOT)/components/ble/peer_manager/peer_data_storage.c \
  $(SDK_ROOT)/components/ble/peer_manager/peer_database.c \
  $(SDK_ROOT)/components/ble/peer_manager/peer_id.c \
  $(SDK_ROOT)/components/ble/peer_manager/peer_manager.c \
  $(SDK_ROOT)/components/ble/peer_manager/pm_buffer.c \
  $(SDK_ROOT)/components/ble/peer_manager/pm_mutex.c \
  $(SDK_ROOT)/components/ble/peer_manager/security_dispatcher.c \
  $(SDK_ROOT)/components/ble/peer_manager/security_manager.c \
  $(SDK_ROOT)/components/ble/ble_services/ble_dfu/ble_dfu.c \
  $(SDK_ROOT)/components/ble/ble_services/ble_dfu/ble_dfu_bonded.c \
  $(SDK_ROOT)/components/ble/ble_services/ble_dfu/ble_dfu_unbonded.c \
  $(SDK_ROOT)/components/toolchain/gcc/gcc_startup_nrf52810.S \
  $(SDK_ROOT)/components/toolchain/system_nrf52810.c \
  $(SDK_ROOT)/components/softdevice/common/nrf_sdh.c \
  $(SDK_ROOT)/components/softdevice/common/nrf_sdh_ble.c \
  $(SDK_ROOT)/components/softdevice/common/nrf_sdh_soc.c \

# Include folders common to all targets
INC_FOLDERS += \
  $(SDK_ROOT)/components \
  $(SDK_ROOT)/components/libraries/scheduler \
  $(SDK_ROOT)/components/libraries/experimental_log \
  $(SDK_ROOT)/components/toolchain/cmsis/include \
  $(SDK_ROOT)/components/libraries/pwr_mgmt \
  $(SDK_ROOT)/components/libraries/strerror \
  $(SDK_ROOT)/components/drivers_nrf/delay \
  $(SDK_ROOT)/components/libraries/crc16 \
  $(SDK_ROOT)/components/libraries/bootloader/dfu \
  $(SDK_ROOT)/components/softdevice/s132/headers/nrf52 \
  $(SDK_ROOT)/components/libraries/util \
  $(SDK_ROOT)/components/drivers_nrf/uart \
  $(SDK_ROOT)/components/ble/common \
  $(SDK_ROOT)/components/libraries/balloc \
  $(SDK_ROOT)/components/ble/peer_manager \
  $(SDK_ROOT)/components/libraries/bsp \
  $(SDK_ROOT)/components/device \
  $(SDK_ROOT)/components/libraries/timer \
  $(SDK_ROOT)/components/ble/nrf_ble_gatt \
  $(SDK_ROOT)/components/libraries/button \
  $(SDK_ROOT)/components/libraries/fstorage \
  $(SDK_ROOT)/components/libraries/experimental_section_vars \
  $(SDK_ROOT)/components/softdevice/s132/headers \
  $(SDK_ROOT)/components/libraries/mutex \
  $(SDK_ROOT)/components/libraries/experimental_log/src \
  $(SDK_ROOT)/components/drivers_nrf/gpiote \
  $(SDK_ROOT)/external/segger_rtt \
  $(SDK_ROOT)/components/libraries/atomic_fifo \
  $(PROJ_DIR)/config \
  $(SDK_ROOT)/components/libraries/atomic \
  $(SDK_ROOT)/components/boards \
  $(SDK_ROOT)/components/drivers_nrf/hal \
  $(SDK_ROOT)/components/libraries/experimental_memobj \
  $(SDK_ROOT)/components/toolchain/gcc \
  $(SDK_ROOT)/components/toolchain \
  $(SDK_ROOT)/components/libraries/fds \
  $(SDK_ROOT)/components/drivers_nrf/common \
  $(SDK_ROOT)/components/ble/ble_advertising \
  $(SDK_ROOT)/components/drivers_nrf/clock \
  $(SDK_ROOT)/components/softdevice/common \
  $(SDK_ROOT)/components/ble/ble_services/ble_dfu \
  $(SDK_ROOT)/external/fprintf \
  $(SDK_ROOT)/components/libraries/svc \

# Libraries common to all targets
LIB_FILES += \

# Optimization flags
OPT = -O3 -g3
# Uncomment the line below to enable link time optimization
#OPT += -flto

# C flags common to all targets
CFLAGS += $(OPT)
CFLAGS += -DBL_SETTINGS_ACCESS_ONLY
CFLAGS += -DAPP_TIMER_V2
CFLAGS += -DAPP_TIMER_V2_RTC1_ENABLED
CFLAGS += -DBOARD_PCA10040
CFLAGS += -DCONFIG_GPIO_AS_PINRESET
CFLAGS += -DFLOAT_ABI_SOFT
CFLAGS += -DDEVELOP_IN_NRF52832
CFLAGS += -DNRF52810_XXAA
CFLAGS += -DNRF52_PAN_74
CFLAGS += -DNRF_DFU_SVCI_ENABLED
CFLAGS += -DNRF_DFU_TRANSPORT_BLE=1
CFLAGS += -DNRF_SD_BLE_API_VERSION=6
CFLAGS += -DS112
CFLAGS += -DSOFTDEVICE_PRESENT
CFLAGS += -DSVC_INTERFACE_CALL_AS_NORMAL_FUNCTION
CFLAGS += -DuECC_ENABLE_VLI_API=0
CFLAGS += -DuECC_OPTIMIZATION_LEVEL=3
CFLAGS += -DuECC_SQUARE_FUNC=0
CFLAGS += -DuECC_SUPPORT_COMPRESSED_POINT=0
CFLAGS += -DuECC_VLI_NATIVE_LITTLE_ENDIAN=1
CFLAGS += -mcpu=cortex-m4
CFLAGS += -mthumb -mabi=aapcs
CFLAGS += -Wall -Werror
CFLAGS += -mfloat-abi=soft
# keep every function in a separate section, this allows linker to discard unused ones
CFLAGS += -ffunction-sections -fdata-sections -fno-strict-aliasing
CFLAGS += -fno-builtin -fshort-enums
CFLAGS += -D__HEAP_SIZE=0

# C++ flags common to all targets
CXXFLAGS += $(OPT)

# Assembler flags common to all targets
ASMFLAGS += -g3
ASMFLAGS += -mcpu=cortex-m4
ASMFLAGS += -mthumb -mabi=aapcs
ASMFLAGS += -mfloat-abi=soft
ASMFLAGS += -DBL_SETTINGS_ACCESS_ONLY
ASMFLAGS += -DAPP_TIMER_V2
ASMFLAGS += -DAPP_TIMER_V2_RTC1_ENABLED
ASMFLAGS += -DBOARD_PCA10040
ASMFLAGS += -DCONFIG_GPIO_AS_PINRESET
ASMFLAGS += -DFLOAT_ABI_SOFT
ASMFLAGS += -DNRF52
ASMFLAGS += -DNRF52810_XXAA
ASMFLAGS += -DNRF52_PAN_74
ASMFLAGS += -DNRF_DFU_SVCI_ENABLED
ASMFLAGS += -DNRF_DFU_TRANSPORT_BLE=1
ASMFLAGS += -DNRF_SD_BLE_API_VERSION=6
ASMFLAGS += -DS112
ASMFLAGS += -DSOFTDEVICE_PRESENT
ASMFLAGS += -DSVC_INTERFACE_CALL_AS_NORMAL_FUNCTION
ASMFLAGS += -DuECC_ENABLE_VLI_API=0
ASMFLAGS += -DuECC_OPTIMIZATION_LEVEL=3
ASMFLAGS += -DuECC_SQUARE_FUNC=0
ASMFLAGS += -DuECC_SUPPORT_COMPRESSED_POINT=0
ASMFLAGS += -DuECC_VLI_NATIVE_LITTLE_ENDIAN=1
ASMFLAGS += -D__HEAP_SIZE=0

# Linker flags
LDFLAGS += $(OPT)
LDFLAGS += -mthumb -mabi=aapcs -L$(SDK_ROOT)/components/toolchain/gcc -T$(LINKER_SCRIPT)
LDFLAGS += -mcpu=cortex-m4
LDFLAGS += -mfloat-abi=soft
# let linker dump unused sections
LDFLAGS += -Wl,--gc-sections
# use newlib in nano version
LDFLAGS += --specs=nano.specs

# Add standard libraries at the very end of the linker input, after all objects
# that may need symbols provided by these libraries.
LIB_FILES += -lc -lnosys -lm

.PHONY: default help

# Default target - first one defined
default: $(TARGET_NAME)

# Print all targets that can be built
help:
	@echo following targets are available:
	@echo		$(TARGET_NAME)
	@echo		flash_softdevice
	@echo		sdk_config - starting external tool for editing sdk_config.h
	@echo		flash      - flashing binary

# Download sdk and install any dependencies
SDK_TEMP := .nrf_sdk.zip
sdk:
	@echo Installing NRF SDK...
	@if [ ! -d $(SDK_ROOT) ]; then \
		if [ ! -f $(SDK_TEMP) ]; then \
			echo Downloading sdk...; \
			curl -o $(SDK_TEMP) $(NRF_SDK_URL); \
	  fi; \
		if [ "`md5 -q $(SDK_TEMP)`" != "$(NRF_SDK_MD5)" ]; then \
			echo SDK Archive is invalid, delete and reinstall deps; \
			exit 1; \
		fi; \
		unzip $(SDK_TEMP) -d $(SDK_ROOT); \
	fi
	@echo Copying toolchain configuration file...
	@cp -f $(SDK_CONFIG)/Makefile.posix $(SDK_ROOT)/components/toolchain/gcc/
	@cp -f $(SDK_CONFIG)/nrf_dfu_types.h $(SDK_ROOT)/components/libraries/bootloader/dfu/
	@echo SDK install complete.
	@rm -f $(SDK_TEMP)

sdk_clean:
	@rm -fr $(SDK_ROOT)
	@rm -f $(SDK_TEMP)

deps: sdk

TEMPLATE_PATH := $(SDK_ROOT)/components/toolchain/gcc

ifeq ($(filter $(MAKECMDGOALS),deps sdk sdk_clean),)
    ifneq ($(MAKECMDGOALS),sdk)
        include $(TEMPLATE_PATH)/Makefile.common
    endif
endif

$(foreach target, $(TARGETS), $(call define_target, $(target)))

.PHONY: settings image flash flash_softdevice app erase sdk_config

settings: $(OUTPUT_DIRECTORY)/$(TARGET_NAME).hex
	nrfutil settings generate --family NRF52 --application $< --application-version-string $(VER_STRING) --bootloader-version 1 --bl-settings-version 1 $(OUTPUT_DIRECTORY)/settings.hex
	@echo Applying settings to the bootloader hex
	mergehex -m $(BL_BIN_DIR)/$(BL_NAME).hex $(OUTPUT_DIRECTORY)/settings.hex -o $(OUTPUT_DIRECTORY)/$(BL_NAME).hex

# Create the combined soft drivce + bootloader
# Then, combine the App with the above
image: settings
	@echo Building combined image: $(OUTPUT_DIRECTORY)/$(TARGET_NAME).combined.hex
	@echo Combining Bootloader with the SoftDevice
	mergehex -m $(SOFT_DEVICE) $(OUTPUT_DIRECTORY)/$(BL_NAME).hex -o $(OUTPUT_DIRECTORY)/$(BL_NAME)_sd.hex
	@echo Combining Application to the SD and Bootloader
	mergehex -m $(OUTPUT_DIRECTORY)/$(BL_NAME)_sd.hex $(OUTPUT_DIRECTORY)/$(TARGET_NAME).hex -o $(OUTPUT_DIRECTORY)/$(TARGET_NAME).combined.hex

# Flash the program
flash: image
	@echo Flashing: $(OUTPUT_DIRECTORY)/$(TARGET_NAME).combined.hex
	nrfjprog -s $(PROG_SER) -f nrf52 --program $(OUTPUT_DIRECTORY)/$(BL_NAME)_sd.hex --sectorerase
	nrfjprog -s $(PROG_SER) -f nrf52 --program $(OUTPUT_DIRECTORY)/$(TARGET_NAME).hex --sectorerase
	nrfjprog -s $(PROG_SER) -f nrf52 --reset

# Flash softdevice
flash_softdevice:
	@echo Flashing: softdevice hex
	nrfjprog -f nrf52 --program $(SOFT_DEVICE) --sectorerase
	nrfjprog -f nrf52 --reset

app: $(OUTPUT_DIRECTORY)/$(TARGET_NAME).hex
	nrfutil pkg generate --sd-req 0xA5,0xA7 --hw-version 52 --key-file $(DFU_DIR)/dfu_private_key.pem \
		--application-version-string $(VER_STRING) --application $< \
		$(OTA_DIR)/$(TARGET_NAME)_app.zip

erase:
	nrfjprog -f nrf52 --eraseall

# Includ getting version from version.json:
include $(MAKE_PATH)/Makefile.debug

SDK_CONFIG_FILE := $(PROJ_DIR)/config/sdk_config.h
CMSIS_CONFIG_TOOL := $(SDK_ROOT)/external_tools/cmsisconfig/CMSIS_Configuration_Wizard.jar
sdk_config:
	java -jar $(CMSIS_CONFIG_TOOL) $(SDK_CONFIG_FILE)
